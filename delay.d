#!/usr/sbin/dtrace -qs

#pragma D option defaultargs

BEGIN
{
	self->count = 0;
	printf("Tracing Started... Ctrl+C to stop\n\n");
	min_ns = $1 ? $1 * 1000000 : 10 * 1000000;
}

fbt::dmu_tx_delay:entry
{
	self->ts = timestamp;
}
fbt::dmu_tx_delay:return
/self->ts/
{
        self->ts = 0;
}

fbt::cv_timedwait_hires:entry
/self->ts/
{
	self->wakeup = (uint64_t) arg2;
	self->cv = timestamp;
	self->vcv = vtimestamp;
	self->nr = curthread->t_cpu->cpu_disp->disp_nrunnable;
}


/*
sched:::off-cpu
/self->cv && curlwpsinfo->pr_state == SSLEEP/
{
	self->cpu = cpu;
	self->ts = timestamp;
}
sched:::on-cpu
/self->ts/
{
	@[self->cpu == cpu ?
	    "no CPU migration" : "CPU migration"] =
	    lquantize((timestamp - self->ts) / 1000000, 0, 50, 1);
	self->ts = 0;
	self->cpu = 0;
}
*/

fbt::cv_timedwait_hires:return
/self->cv && (timestamp - self->cv) >= min_ns/ /* && (timestamp - self->wakeup) >= 1000000/ */
{
	this->ts = timestamp;
	printf("\n%d|(%d)|%d(%d)|%s took %d ms (%d over) (%d oncpu) timeout: %d, now: %d",
		cpu, curthread->t_pri, curthread->t_cpu->cpu_disp->disp_nrunnable, self->nr, probefunc,
		(this->ts - self->cv)/1000000, (this->ts - self->wakeup)/1000000, (vtimestamp - self->vcv)/1000000,
		self->wakeup/1000000, this->ts/1000000);
	@co[probefunc, (this->ts - self->cv) / 1000000]=count();
}
fbt::cv_timedwait_hires:return
/self->cv/
{
	self->cv = 0;
	self->vcv = 0;
	self->wakeup = 0;
	self->nr = 0;
}
