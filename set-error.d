#!/usr/sbin/dtrace -qs

/*
 * CDDL HEADER START
 *
 * This file and its contents are supplied under the terms of the
 * Common Development and Distribution License ("CDDL"), version 1.0.
 * You may only use this file in accordance with the terms of version
 * 1.0 of the CDDL.
 *
 * A full copy of the text of the CDDL should have accompanied this
 * source.  A copy of the CDDL is also available via the Internet at
 * http://www.illumos.org/license/CDDL.
 *
 * CDDL HEADER END
 *
 * Copyright 2016 Nexenta Systems Inc.
 *
 */

dtrace:::BEGIN
{
	/*
	 * Error codes from usr/src/uts/common/sys/err.h
	 */

	err[1] = "EPERM - Not super-user";
	err[2] = "ENOENT - No such file or directory";
	err[3] = "ESRCH - No such process";
	err[4] = "EINTR - interrupted system call";
	err[5] = "EIO - I/O error";
	err[6] = "ENXIO - No such device or address";
	err[7] = "E2BIG - Arg list too long";
	err[8] = "BNOEXEC - Exec format error";
	err[9] = "EBADF - Bad file number";
	err[10] = "ECHILD - No children";
	err[11] = "EAGAIN - Resource temporarily unavailable";
	err[12] = "ENOMEM - Not enough core";
	err[13] = "EACCES - Permission denied";
	err[14] = "EFAULT - Bad address";
	err[15] = "ENOTBLK - Block device required";
	err[16] = "EBUSY - Mount device busy";
	err[17] = "EEXIST - File exists";
	err[18] = "EXDEV - Cross-device link";
	err[19] = "ENODEV - No such device";
	err[20] = "ENOTDIR - Not a directory";
	err[21] = "EISDIR - Is a directory";
	err[22] = "EINVAL - Invalid argument";
	err[23] = "ENFILE - File table overflow";
	err[24] = "EMFILE - Too many open files";
	err[25] = "ENOTTY - Inappropriate ioctl for device";
	err[26] = "ETXTBSY - Text file busy";
	err[27] = "EFBIG - File too large";
	err[28] = "ENOSPC - No space left on device";
	err[29] = "ESPIPE - Illegal seek";
	err[30] = "EROFS - Read only file system";
	err[31] = "EMLINK - Too many links";
	err[32] = "EPIPE - Broken pipe";
	err[33] = "EDOM - Math arg out of domain of func";
	err[34] = "ERANGE - Math result not representable";
	err[35] = "ENOMSG - No message of desired type";
	err[36] = "EIDRM - Identifier removed";
	err[37] = "ECHRNG - Channel number out of range";
	err[38] = "EL2NSYNC - Level 2 not synchronized";
	err[39] = "EL3HLT - Level 3 halted";
	err[40] = "EL3RST - Level 3 reset";
	err[41] = "ELNRNG - Link number out of range";
	err[42] = "EUNATCH - Protocol driver not attached";
	err[43] = "ENOCSI - No CSI structure available";
	err[44] = "EL2HLT - Level 2 halted";
	err[45] = "EDEADLK - Deadlock condition.";
	err[46] = "ENOLCK - No record locks available.";
	err[47] = "ECANCELED - Operation canceled";
	err[48] = "ENOTSUP - Operation not supported";

	/* Filesystem Quotas */
	err[49] = "EDQUOT - Disc quota exceeded";

	/* Convergent Error Returns */
	err[50] = "EBADE - invalid exchange";
	err[51] = "EBADR - invalid request descriptor";
	err[52] = "EXFULL - exchange full";
	err[53] = "ENOANO - no anode";
	err[54] = "EBADRQC - invalid request code";
	err[55] = "EBADSLT - invalid slot";
	err[56] = "EDEADLOCK - file locking deadlock error";

	err[57] = "EBFONT - bad font file fmt";

	/* Interprocess Robust Locks */
	err[58] = "EOWNERDEAD - process died with the lock";
	err[59] = "ENOTRECOVERABLE - lock is not recoverable";

	/* stream problems */
	err[60] = "Device not a stream";
	err[61] = "no data (for no delay io)";
	err[62] = "timer expired";
	err[63] = "out of streams resources";

	err[64] = "Machine is not on the network";
	err[65] = "Package not installed";
	err[66] = "The object is remote";
	err[67] = "the link has been severed";
	err[68] = "advertise error";
	err[69] = "srmount error";

	err[70] = "Communication error on send";
	err[71] = "Protocol error";

	/* Interprocess Robust Locks */
	err[72] = "locked lock was unmapped";

	err[73] = "Facility is not active";
	err[74] = "multihop attempted";

	err[77] = "trying to read unreadable message";
	err[78] = "path name is too long";
	err[79] = "value too large to be stored in data type";
	err[80] = "given log. name not unique";
	err[81] = "f.d. invalid for this operation";
	err[82] = "Remote address changed";

	/* shared library problems */
	err[83] = "ELIBACC - Can't access a needed shared lib.";
	err[84] = "ELIBBAD - Accessing a corrupted shared lib.";
	err[85] = "ELIBSCN - .lib section in a.out corrupted.";
	err[86] = "ELIBMAX - Attempting to link in too many libs.";
	err[87] = "ELIBEXEC - Attempting to exec a shared library.";
	err[88] = "EILSEQ - Illegal byte sequence.";
	err[89] = "ENOSYS - Unsupported file system operation";
	err[90] = "ELOOP - Symbolic link loop";
	err[91] = "ERESTART - Restartable system call";
	err[92] = "ESTRPIPE - if pipe/FIFO, don't sleep in stream head";
	err[93] = "ENOTEMPTY - directory not empty";
	err[94] = "EUSERS - Too many users (for UFS)";

	/* BSD Networking Software */
		/* argument errors */
	err[95] = "Socket operation on non-socket";
	err[96] = "Destination address required";
	err[97] = "Message too long";
	err[98] = "Protocol wrong type for socket";
	err[99] = "Protocol not available";
	err[120] = "Protocol not supported";
	err[121] = "Socket type not supported";
	err[122] = "Operation not supported on socket";
	err[123] = "Protocol family not supported";
	err[124] = "Address family not supported by";
		/* protocol family */
	err[125] = "Address already in use";
	err[126] = "Can't assign requested address";
		/* operational errors */
	err[127] = "Network is down";
	err[128] = "Network is unreachable";
	err[129] = "Network dropped connection because";
		/* of reset */
	err[130] = "Software caused connection abort";
	err[131] = "Connection reset by peer";
	err[132] = "No buffer space available";
	err[133] = "Socket is already connected";
	err[134] = "Socket is not connected";

	/* XENIX has 135 - 142 */
	err[143] = "Can't send after socket shutdown";
	err[144] = "Too many references: can't splice";
	err[145] = "Connection timed out";
	err[146] = "Connection refused";
	err[147] = "Host is down";
	err[148] = "No route to host";
	err[149] = "operation already in progress";
	err[150] = "operation now in progress";

	/* SUN Network File System */
	err[151] = "ESTALE - Stale NFS file handle"
}

zfs::set-error
{
	@error[probefunc, arg0, err[arg0]] = count();
}

fbt::zfsdev_ioctl:return
/arg1 != 0/
{
        @error[probefunc, arg1, err[arg1]] = count();
}

profile:::tick-$1s
{
	printf("\n%Y",walltimestamp);
	printa(@error);
	trunc(@error);
}
