#!/usr/sbin/dtrace -qFs


#pragma D option dynvarsize=64m

fbt::spa_sync:entry
/stringof((args[0])->spa_name) == $$1/
{
	self->txg = arg1;
	spa = args[0];
	self->sync_start = timestamp;
	printf("%s txg: %d\n",probefunc,arg1);
}

:::tick-10000000ms
/spa/
{
        @dirty_mb = max((((spa->spa_dsl_pool)->dp_dirty_total) / 0x400) / 0x400);
	printf("dirty now %d MB\n", ((spa->spa_dsl_pool)->dp_dirty_total / 0x400) / 0x400);
}

ddt-syncing-avl
/spa/
{
	self->avl = timestamp;
}
fbt::ddt_sync_entry:entry
/self->txg == arg3/
{
        @ddes_per_txg=count();
}
ddt-synced-avl
/self->avl/
{
	printf("%s took %d ms\n",probefunc,(timestamp-self->avl)/1000000);
	self->avl = 0;
}

fbt::dsl_pool_sync:entry
/arg1 == self->txg/
{
	self->obj = timestamp;
	printf("%s\n",probefunc);
}
fbt::dsl_pool_sync_mos:entry
/self->obj/
{
	self->o = timestamp;
	printf("%s\n",probefunc);
}
fbt::dnode_sync:entry
/self->o && 0x2a == args[0]->dn_type/
{
	this->dn = args[0];
	self->dnode = timestamp;
	printf("dnode type: DMU_OT_DDT_ZAP, dn_dbufs_count: %d\n ",this->dn->dn_dbufs_count);
}
fbt::dnode_sync:return
/self->dnode/
{
	printf("%s sync pass: %d, took %d ms\n",probefunc,spa->spa_sync_pass,(timestamp-self->dnode)/1000000);
	self->dnode = 0;

}
fbt::dsl_pool_sync_mos:return
/self->o/
{
	printf("%s sync pass: %d, took %d ms\n",probefunc,spa->spa_sync_pass,(timestamp-self->o)/1000000);
	self->o = 0;
}
fbt::dsl_pool_sync:return
/self->obj/
{
	printf("%s sync pass: %d, took %d ms\n",probefunc,spa->spa_sync_pass,(timestamp-self->obj)/1000000);
	self->obj = 0;
}

fbt::spa_sync:return
/self->sync_start!=0/
{
	printf("txg %d ",self->txg);
	printa("had %@6u - DDEs, marked max of %@4uMB as dirty and ", @ddes_per_txg, @dirty_mb);
	printf("took: %d ms\n\n",(timestamp - self->sync_start)/1000000);

	self->sync_start=0;
	self->txg = 0;
	spa = 0;
	clear(@dirty_mb);
	clear(@ddes_per_txg);
}

